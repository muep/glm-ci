#!/bin/sh

exit_ok=0
exit_usage=1

usage() {
    cat<<EOF
usage: $1 [OPTIONS]... <PATH_TO_GLM>
options:
    -h       this help
    -s       C++ standard version
    -t       build type
EOF
}

buildtype=""
incremental=""
standard=""

while getopts "his:t:" opt
do
    case "${opt}" in
        h)
            usage
            exit "${exit_ok}"
            ;;
        i)
            incremental=yes
            ;;
        s)
            standard="${OPTARG}"
            ;;
        t)
            buildtype="${OPTARG}"
            ;;
        *)
            2>&1 usage
            exit "${exit_usage}"
            ;;
    esac
done

shift $((OPTIND - 1))

set -xe

if test ${#} -ne 1
then
    2>&1 usage
    exit "${exit_usage}"
fi
glmpath="${1}"
parallel="-j$(nproc)"

CXXFLAGS=""

if test -z "${buildtype}"
then
    # Let's follow GLM defaults as closely as we can.
    btype_arg=""
elif test debug = "${buildtype}"
then
    btype_arg="-DCMAKE_BUILD_TYPE=Debug"
    CXXFLAGS="-Wall"
elif test release = "${buildtype}"
then
    btype_arg="-DCMAKE_BUILD_TYPE=RelWithDebInfo"
    CXXFLAGS="-Wall"
fi

CFLAGS="${CXXFLAGS}"

if test -n "${standard}"
then
    CXXFLAGS="${CXXFLAGS} -std=${standard}"
fi

if test -n "${CXXFLAGS}"
then
    export CFLAGS
    export CXXFLAGS
fi

cd "${glmpath}"

if ! test "${incremental}" = yes
then
    rm -rf build
fi

mkdir -pv build
cd build
cmake -DGLM_TEST_ENABLE=ON ${btype_arg} ..
make VERBOSE=1 ${parallel}
ctest --output-on-failure ${parallel}
