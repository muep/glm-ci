#!/bin/sh

set -xe

if test ${#} -ne 1
then
    echo "usage: ${0} <path_to_glm>"
    exit 1
fi

glmpath="${1}"
parallel="-j$(nproc)"

cd "${glmpath}"
mkdir -pv build
cd build
cmake -DGLM_TEST_ENABLE=ON ..
make VERBOSE=1 ${parallel}
ctest --output-on-failure ${parallel}
