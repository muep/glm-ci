#!/bin/sh

set -e

EXIT_OK=0
EXIT_DIFFERENCE=1
EXIT_USAGE=80
EXIT_GITREPO=81
EXIT_ZIP_MISSING=82

if test ${#} -ne 2
then
    echo "usage: ${0} <git repository root> <glm-something.zip>"
    exit $EXIT_USAGE
fi

git_repo_root="$1"
zipfile="${2}"

# This might be a bit fragile.
version="$(basename -s.zip "${zipfile}"|sed s/^glm-//)"

if ! test -d "${git_repo_root}/.git"
then
    echo "expected git repository in ${git_repo_root}"
    exit EXIT_GITREPO
fi

if ! test -e "${zipfile}"
then
    echo "${zipfile} does not exist"
    exit EXIT_ZIP_MISSING
fi

env GIT_DIR="${git_repo_root}/.git" \
    git archive --format=tar --prefix="glm-${version}-git/" "${version}" . | tar x

unzip -q -d "glm-${version}" "${zipfile}"
mv "glm-${version}/glm" "glm-${version}-zip"
rmdir "glm-${version}"

if diff -ru --strip-trailing-cr "glm-${version}-git" "glm-${version}-zip"
then
    rv=${EXIT_OK}
else
    rv=${EXIT_DIFFERENCE}
fi

rm -rf "glm-${version}-git" "glm-${version}-zip"
exit "${rv}"
